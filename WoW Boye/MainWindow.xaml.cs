﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WindowsInput;
using System.Timers;

namespace WoW_Boye
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        InputSimulator sim = new InputSimulator();
        Timer timer = new Timer(5000);
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnStartStop_Click(object sender, RoutedEventArgs e)
        {
            if (btnStartStop.Content.ToString() == "Start")
            {
                // setup ui
                btnStartStop.Content = "Stop";
                lblStatus.Content = "Enabled";
                m_imgNotRunning.Visibility = Visibility.Hidden;

                // init timer
                timer.Elapsed += AutoClick;
                timer.Interval = 5000;
                timer.Enabled = true;
            }
            else
            {
                // setup ui
                btnStartStop.Content = "Start";
                lblStatus.Content = "Disabled";
                m_imgNotRunning.Visibility = Visibility.Visible;

                // stop timer
                timer.Enabled = false;
                timer.Elapsed -= AutoClick;
            }
        }

        void AutoClick(object source, ElapsedEventArgs e)
        {
            // simulate mouse clicks for 0.1s
            sim.Mouse.RightButtonDown();
            sim.Mouse.LeftButtonDown();
            sim.Mouse.Sleep(100);
            sim.Mouse.RightButtonUp();
            sim.Mouse.LeftButtonUp();
        }
    }
}
